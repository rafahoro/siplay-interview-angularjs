const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

    target: 'web',

    entry: {
        app: ['./src/bootstrap.js']
    },

    output: {
        publicPath: '/',
        filename: '[name].[hash:8].js',
        sourceMapFilename: '[file].map'
    },

    resolve: {
        extensions: ['.js', '.json', '.css', '.scss', '.pug', '.html']
    },

    devtool: 'source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.pug$/,
                use: ['raw-loader', 'pug-html-loader']
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader?sourceMap'
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader?sourceMap!sass-loader?sourceMap'
                })
            },
            {
                test: /\.(ico)$/,
                use: 'file-loader?name=[name].[ext]'
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                use: 'file-loader?name=img/[name].[ext]'
            },
            {
                test: /\.(ttf|woff|woff2|eot|svg)$/,
                use: 'file-loader?name=font/[name].[ext]'
            },
            {
                test: /\.md$/,
                loaders: ['html-loader', 'markdown-loader']
            }
        ]
    },

    plugins: [
        new webpack.ProgressPlugin(),

        new webpack.optimize.OccurrenceOrderPlugin(true),

        new ExtractTextPlugin('style.[hash:8].css'),

        new HtmlWebpackPlugin({
            cache: false,
            template: './src/index.pug',
            filename: 'index.html',
            chunksSortMode: 'dependency',
            favicon: './src/favicon.ico'
        }),

        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            'window.Tether': 'tether'
        })
    ]
};