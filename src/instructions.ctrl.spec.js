import InstructionsCtrl from './instructions.ctrl';

describe('InstructionsCtrl', () => {

    let module;

    beforeEach(() => {
        module = angular.module('app', ['ngRoute']);
        angular.mock.module('app');
        InstructionsCtrl.registerDirective(module);
    });

    describe('.ctr', () => {

        let ctrl;

        beforeEach(() => {
            angular.mock.inject($controller => {
                ctrl = $controller('InstructionsCtrl');
            });
        });

        it('can instantiate', () => {
        });

    });

});