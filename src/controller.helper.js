export default {

    /**
     * Creates a helper function that registers a controller/directive combination.
     * @param selector - the selector that will be used to register the directive; also the name of the template file
     * @param ctrl - a reference to the controller class
     * @param template
     * @param [config]
     */
    registerDirective: (selector, ctrl, template, config) => ctrl.registerDirective = ngModule => ngModule
        .controller(ctrl.name, ctrl)
        .directive(selector, () => angular.extend({
            template,
            controller: ctrl.name,
            controllerAs: selector
        }, config || {})),

    registerRoute: (path, ctrl, template) => ctrl.registerRoute = ngModule => ngModule
        .controller(ctrl.name, ctrl)
        .config($routeProvider => {
            $routeProvider.when(path, {
                template: template,
                controller: ctrl.name
            })
        })
};