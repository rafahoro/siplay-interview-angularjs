import helper from '../controller.helper';

import './doc.scss';

const docs = [
    { name: 'overview', template: require('@siplay/interview-docs/src/OVERVIEW.md') },
    { name: 'get-started', template: require('@siplay/interview-docs/src/GETSTARTED.md') },
    { name: 'exercise', template: require('@siplay/interview-docs/src/EXERCISE.md') },
    { name: 'dev-api', template: require('@siplay/simple-rest-api/API.md') }
];

function docFactory(ngModule) {
    return doc => {
        function DocCtrl() {
        }

        helper.registerRoute(`/docs/${doc.name}`, DocCtrl, `<doc>${doc.template}</doc>`);
        DocCtrl.registerRoute(ngModule);
    };
}

export default {
    registerRoute: ngModule => docs.forEach(docFactory(ngModule))
}