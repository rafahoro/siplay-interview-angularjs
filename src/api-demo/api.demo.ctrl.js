import helper from '../controller.helper';

const BASE_URL = 'http://localhost:3000/api';
const REQUEST_BODY_METHODS = ['POST', 'PUT', 'PATCH'];

class ApiRequest {

    constructor(oldRequest) {
        this.path = '';
        this.headers = oldRequest ? oldRequest.headers : {};
    }

}

export default class ApiDemoCtrl {

    constructor($http, $scope) {
        this.$http = $http;
        this.$scope = $scope;
        this.$scope.request = new ApiRequest();
        this.$scope.send = request => this.send(request);
        this.$scope.hasRequestBody = method => this.hasRequestBody(method);

        $scope.$watch('request.path', value => {
            if (!value) {
                return;
            }
            $scope.request.url = `${BASE_URL}${value.match(/^\//) ? '' : '/'}${value}`;
        });
    }

    send(request) {
        this.$http(request)
            .then(
                response => this.$scope.response = response,
                response => this.$scope.response = response
            );
    }

    hasRequestBody(method) {
        return REQUEST_BODY_METHODS.indexOf(method) >= 0;
    }
}

helper.registerRoute('/api-demo', ApiDemoCtrl, require('./api.demo.pug'));