import angular from 'angular';
import 'angular-route';
import 'angular-legacy-sortablejs-maintained';

import controllers  from './controllers';

import './style.scss';

const MODULE_NAME = 'app';

const ngModule = angular.module(MODULE_NAME, ['ngRoute','ng-sortable'])
  .config(($routeProvider, $locationProvider) => {
    $locationProvider.html5Mode(true);
  });

controllers.forEach(module => {
  if (module.default.registerDirective) {
    module.default.registerDirective(ngModule);
  }
  if (module.default.registerRoute) {
    module.default.registerRoute(ngModule);
  }
});

export default MODULE_NAME;