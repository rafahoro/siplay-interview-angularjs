import HomeCtrl from './home.ctrl';

describe('HomeCtrl', () => {

    let module;

    beforeEach(() => {
        module = angular.module('app', ['ngRoute']);
        angular.mock.module('app');
        HomeCtrl.registerRoute(module);
    });

    describe('.ctr', () => {

        let ctrl;

        beforeEach(() => {
            angular.mock.inject($controller => {
                ctrl = $controller('HomeCtrl');
            });
        });

        it('can instantiate', () => {
        });

    });

});