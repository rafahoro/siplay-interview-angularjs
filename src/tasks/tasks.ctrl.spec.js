import TasksCtrl from './tasks.ctrl';

describe('TasksCtrl', () => {

    let module;

    beforeEach(() => {
        module = angular.module('app', ['ngRoute']);
        angular.mock.module('app');
        TasksCtrl.registerRoute(module);
    });

    describe('.ctr', () => {

        let ctrl;

        beforeEach(() => {
            angular.mock.inject($controller => {
                ctrl = $controller('TasksCtrl');
            });
        });

        it('sets the `instantiated property', () => {
            expect(ctrl.instantiated).toBe(true);
        });

    });

    // TODO: Add your tests here

});