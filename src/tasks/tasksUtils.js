const BASE_URL = 'http://localhost:3000/api';
const AUTHORIZATION = 'default_user';
const TASKS_URL = BASE_URL + '/tasks';

const HEADERS = {
  Authorization: AUTHORIZATION,
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

function addTask (task){
  const body = JSON.stringify(task);
  return fetch(TASKS_URL, {
    method: 'POST',
    headers: HEADERS,
    body: body
  })
    .then(response => response.text());
}

function getTasks (){
  return fetch(TASKS_URL, {
    method: 'GET',
    headers: HEADERS,
    body: null
  })
    .then(response => response.json())
    .then(json =>{
      return json;
    })
    .catch(e =>{
      console.error("Probably the /tasks path doesn't exist yet. ", e);// 'console' not a good practice, but for POC.
      return [];
    });
}

function patchTask (url, change){
  const body = JSON.stringify(change);
  const init = {
    method: 'PATCH',
    body: body,
    headers: HEADERS
  };
  return fetch(url, init, body)
    .then(response => response.json());
}

function deleteTask (task){
  const url = task['_links'].self;
  return fetch(url, {
    method: 'DELETE',
    headers: HEADERS,
    body: null
  })
}

export {addTask, getTasks, patchTask, deleteTask};
