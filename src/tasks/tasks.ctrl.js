import helper from '../controller.helper';
import * as Utils from './tasksUtils';

import './tasks.scss';

export default class TasksCtrl {

  constructor ($scope, $timeout){
    this.instantiated = true;
    this.$scope = $scope;
    this.$timeout = $timeout;

    // Used to dis/allow some user events while waiting for backend
    this.$scope.isLoading = false; // we are reading the list of tasks from the DB/API
    this.$scope.isReordering = false; // The order (or amount) of tasks is changing.
    //this.$scope.isUpdating = false; // See comment on 'updateTask()' to see why this is not used

    // Model and Methods to be called by HTML
    this.$scope.tasks = [];
    this.$scope.addTask = () => this.addTask();
    this.$scope.deleteTask = (task) => this.deleteTask(task);
    this.$scope.updateTaskStatus = (task) => this.updateTaskStatus(task);
    this.$scope.updateTaskText = (task) => this.updateTaskText(task);
    this.$scope.editingTask = (task) => this.editingTask(task);

    // Drag-and-Drop (used by the user to sort tasks) configuration
    this.$scope.sortableConf = {
      animation: 150,
      handle: ".dnd-handle",
      onEnd: (evt) => this.onEndSort(evt)
    };
    this.getTasks();
  }


  getTasks (){
    this.$scope.isLoading = true;
    return Utils.getTasks()
      .then(tasks =>{
        this.$scope.$apply(() =>{
          this.$scope.tasks = tasks.sort((tsk1, tsk2) =>{
            return (tsk1.order - tsk2.order)
          });
          this.$scope.isLoading = false;
        });
      })
      .catch(e =>{
        console.error(e);// bad practice, but it's only POC and we do not have a logging facility
        this.$scope.isLoading = false;
      });
  }

  addTask (){
    if (!this.$scope.taskText || this.$scope.taskText.trim().length < 1){
      return
    }
    this.$scope.isReordering = true;
    return Utils.addTask({text: this.$scope.taskText, done: false, order: this.$scope.tasks.length})
      .then(() =>{
        // this.$timeout(() =>{
        // adding the task succeded. Let clear the input box
        this.$scope.taskText = '';
        this.$scope.isReordering = false;
        // }, 3000);
        return this.getTasks();
      })
      .catch(e =>{
        console.error(e);// bad practice, but it's only POC and we do not have a logging facility
        this.$scope.isReordering = false;
      })
  }

  deleteTask (task){
    this.$scope.isReordering = true;
    this.$scope.tasks.splice(task.order, 1);

    return Utils.deleteTask(task)
      .then(() => this.fixTaskOrder())
      .then(() => this.getTasks())
      .then(() =>{
        // this.$timeout(() =>{
        this.$scope.isReordering = false;
        // }, 3000);
      })
      .catch(e =>{
        // Sadly we do not have transactions in the API. Read the comment on fixTaskOrder() method below.
        console.error(e);// bad practice, but it's only POC and we do not have a logging facility
        alert('There was an error deleting the task. Please try again if necessary'); // ALERT is ugly, but for POC :)
        this.$scope.isReordering = false;
      })
  }

  /**
   * Show a task's text as editable field
   * @param task
   */
  editingTask (idx){
    this.$scope.editingTaskIdx = idx;
    this.$timeout(function (){
      $("#taskText-" + idx).focus();
    }, 0);
  }

  /**
   * Patches/modifies 1 or more fields of a task on the DB.
   * Note that we are not using a this.$scope.isUpdating field, that can be used to hold/wait other operations.
   * In theory there can be a bug:
   *  1- The user does an update to a field in Task-1
   *  2- Somehow the message to the DB/API is not sent yet.
   *  3- The user deletes Task-1
   *  4- The message to delete Task-1 is sent and processed by the DB/API
   *  5- The message to update a field in Task-1 is sent to the DB/API and fails (Task-1 was just deleted).
   *  Although that is theorically possible, what will happend is just an error logged, but no real damage.
   *  So, will ignore that race condition to mantain the code as simple as possible
   * @param url
   * @param change
   * @returns {Promise.<T>|*}
   */
  updateTask (url, change){
    return Utils.patchTask(url, change)
      .catch(e =>{
        console.error(e); // bad proctice, but it's only POC and we do not have a logging facility
      })
  }

  updateTaskText (task){
    return this.updateTask(task['_links'].self, {text: task.text})
  }

  updateTaskStatus (task){
    return this.updateTask(task['_links'].self, {done: task.done});
  }

  updateTaskOrder (task){
    return this.updateTask(task['_links'].self, {order: task.order});
  }

  /**
   * After drag-and-drop or delete a task, the task.order might differ from the index in the tasks array.
   * This method fixes the task.order, to be the current index in the tasks array.
   * It may fix all the array (if no parameters) or only a portion of it (ie: the portion modified by a dragAndDrop).
   * @param from (inclusive)
   * @param to (inclusive)
   */
  fixTaskOrder (from, to){
    from = from || 0;
    to = to || this.$scope.tasks.length - 1;
    const proms = [];
    this.$scope.isReordering = true;
    for (let i = from; i <= to; i++) {
      this.$scope.tasks[i].order = i;
      proms.push(this.updateTaskOrder(this.$scope.tasks[i]));
    }
    return Promise.all(proms)
      .then(() =>{
        this.$scope.isReordering = false;
        this.getTasks();
      }).catch((e) =>{
        /*
        As there is no transactions in this API, we may fail in some task update, while succeding in other. In such case
        we may have 2 tasks with same sort order, or a leap in the sort-order. We could try to fix this by looping on the
        'tasks' array, but then when we save using the API, we may fail again (and try to fix, and fail, and try to fix
        and fail... you get it). So will just let the user know
         */
        console.error(e); // console is bad practice. But it's POC
        alert('There was an error sorting the tasks. Please try again if necessary'); // ALERT is ugly, but for POC :)
        this.$scope.isReordering = false;
      })
  }

  /**
   * This method is called once the user droped the element on it's new place on the list.
   * When this method is called, the $scope.tasks array is already sorted to reflect the new order, but the task.order
   * fields are not updated yet, so we need to do so.
   * @param evt
   */
  onEndSort (evt){
    const min = Math.min(evt.newIndex, evt.oldIndex);
    const max = Math.max(evt.newIndex, evt.oldIndex);
    /*
     The 'angular-legacy-sortablejs-maintained' plugin, modifies our model (the 'tasks' array) so we have it here
     already sorted. We will take advantage of that by just fixing the '.order' field of the tasks that were modified
     and send the update to the DB/API
     */
    this.fixTaskOrder(min, max);
  }
}

helper.registerRoute('/tasks', TasksCtrl, require('./tasks.pug'));
