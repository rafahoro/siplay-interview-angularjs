export default [
    require('./app.ctrl'),

    require('./instructions.ctrl'),

    require('./api-demo/api.demo.ctrl'),
    require('./api-demo/json.validator'),

    require('./docs/doc.ctrl'),
    require('./home/home.ctrl'),
    require('./tasks/tasks.ctrl')
];